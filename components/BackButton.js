import React from "react";
import { TouchableOpacity } from "react-native";
import GlobalStyles from "../constants/GlobalStyles";
import ICONS from "../constants/Icons";
import {useNavigation} from "@react-navigation/native";


const BackButton = () => {
    const navigation = useNavigation();

    return (
        <TouchableOpacity
            style={[GlobalStyles.BackButton, GlobalStyles.Shadow]}
            onPress={() => navigation.goBack()}
        >
            { ICONS.back }
        </TouchableOpacity>
    )
};

export default BackButton;