import React from "react";
import { View, Text } from "react-native";
import GlobalStyles from "../constants/GlobalStyles";
import BackButton from "./BackButton";

const Header = ({title}) => {

    return (
        <View style={GlobalStyles.HeaderContainer}>
            <Text style={GlobalStyles.H1}>
                { title }
            </Text>

            <BackButton />

        </View>
    )
};

export default Header;