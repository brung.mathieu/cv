import React from "react";
import { StyleSheet, View, Text, Image} from "react-native";
import GlobalStyles from "../constants/GlobalStyles";
import COLORS from "../constants/Colors";

const ProjectItem = ({data}) => {

    return (
        <View style={[GlobalStyles.DataWrapper, styles.container]}>
            {/* View top */}
            <View style={[styles.topTitleWrapper, styles.bottomMargin]}>
                <View style={[GlobalStyles.Circle, GlobalStyles.Shadow]} />
                <Text style={GlobalStyles.H3}>{ data.title }</Text>
            </View>

            {/* View middle */}
            <View style={styles.bottomMargin} >

                {/* View title */}
                <View style={[styles.middleTitlesWrapper, styles.bottomMargin]} >
                    <Text style={GlobalStyles.H4}>Technos</Text>
                    <Text style={GlobalStyles.H4}>IDE</Text>
                </View>

                {/* View logos */}
                    <View style={styles.middleLogosWrapper} >

                    {/* View technos logo */}
                        <View style={styles.technosLogosWrapper} >
                            {
                                data.technos.map((item, index) => {
                                    return (
                                        <Image source={item.logo} style={[GlobalStyles.LogoTiny, styles.logoList]} key={index} />
                                    )
                                })
                            }
                        </View>

                    {/* View IDE logo */}
                        <View>
                            <Image source={data.ide} style={GlobalStyles.LogoTiny} />
                        </View>

                    </View>
            </View>

            {/* View bot */}
            <View style={styles.bottomMargin} >
                <View>
                    {/* Sub title */}
                    <Text style={[GlobalStyles.H4, styles.bottomMargin]}>Description</Text>
                    {/* Description */}
                    <Text style={[GlobalStyles.Text, {marginBottom: 5}]}>{data.text}</Text>
                </View>

                {/* API */}
                {
                    data.api !== '' ?
                        <View>
                            <Text style={[GlobalStyles.H4, styles.bottomMargin, {marginBottom: 0}]}>API</Text>
                            <Text style={GlobalStyles.Text}>{data.api}</Text>
                        </View>
                    :
                    null
                }
            </View>

        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        marginBottom: 12,
    },
    topTitleWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    middleTitlesWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginRight: 8,
    },
    middleLogosWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    technosLogosWrapper: {
        flexDirection: 'row'
    },
    logoList: {
        marginRight: 5
    },
    bottomMargin: {
        marginBottom: 5
    }
});

export default ProjectItem;