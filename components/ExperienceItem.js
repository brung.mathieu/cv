import React from "react";
import {StyleSheet, View, Text, Image} from "react-native";
import GlobalStyles from "../constants/GlobalStyles";
import COLORS from "../constants/Colors";

const ExperienceItem = ({data}) => {
    return (
        <View style={styles.itemWrapper}>

            <View style={[styles.circle, GlobalStyles.Shadow]}></View>

            <View style={[GlobalStyles.DataWrapper, styles.itemDataWrapper]}>

                <View style={styles.topWrapper}>
                    {/*Logo*/}
                    <Image source={data.logo} style={GlobalStyles.Logo} />
                    <View style={styles.titlesWrapper}>
                        {/*Title*/}
                        <Text style={GlobalStyles.H3}>{ data.title }</Text>
                        {/*Dates*/}
                        <Text style={GlobalStyles.H4}>{ data.date_start }{ data.date_end !== '' ? ' / ' + data.date_end : null}</Text>
                    </View>
                </View>

                <View style={styles.botWrapper}>
                    {/*Text*/}
                    <Text style={GlobalStyles.Text}>{ data.text }</Text>
                </View>

            </View>

        </View>
    )
};

const styles = StyleSheet.create({
    itemWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 6
    },
    circle: {
        position: 'relative',
        left: 0,
        width: 12,
        height: 12,
        borderColor: COLORS.orange,
        backgroundColor: COLORS.orange,
        borderWidth: 2,
        borderRadius: 6,
        marginHorizontal: 5
    },
    itemDataWrapper: {
        flex: 1,
        marginLeft: 0,
    },
    topWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    titlesWrapper: {
        marginLeft: 8
    },
    botWrapper: {
        marginTop: 5
    }
})

export default ExperienceItem;