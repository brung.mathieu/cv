import React from "react";
import {StyleSheet, View, Text } from 'react-native';
import GlobalStyles from "../constants/GlobalStyles";
import COLORS from "../constants/Colors";

const HobbyItem = ({data}) => {
    return (
        <View style={styles.itemWrapper}>
            {
                data.id !== '1' ?
                    <View style={styles.separator} />
                    : null
            }
            <View>
                <View style={styles.topWrapper}>
                    <View style={styles.topTitleWrapper}>
                        <View style={[GlobalStyles.Circle, GlobalStyles.Shadow]} />
                        <Text style={GlobalStyles.H3}>{ data.title }</Text>
                    </View>
                    {data.icon}
                </View>

                <View style={styles.textWrapper}>
                    <Text style={GlobalStyles.Text} >{ data.text }</Text>
                </View>
            </View>

        </View>
    )
};

const styles = StyleSheet.create({
    itemWrapper: {
        flex: 1,
        flexDirection: 'row',
        paddingHorizontal: 6,
        paddingTop: 4
    },
    separator: {
        alignSelf: 'center',
        height: '100%',
        width: StyleSheet.hairlineWidth,
        backgroundColor: COLORS.gray,
        marginRight: 14,
        marginLeft: 8,
    },
    topWrapper: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    topTitleWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    textWrapper: {
        width: 260,
    }
});

export default HobbyItem;