import React from "react";
import {StyleSheet, TouchableOpacity, Text} from "react-native";
import GlobalStyles from "../constants/GlobalStyles";
import COLORS from "../constants/Colors";
import {useNavigation} from "@react-navigation/native";


const HomeItem = (data) => {
    const navigation = useNavigation();
    const icon = data.data.icon;
    const title = data.data.title;
    const screen = data.data.screenName;

    return (
        <TouchableOpacity
            style={[styles.button, GlobalStyles.Shadow]}
            onPress={() => navigation.navigate(screen)}
        >
            <Text style={GlobalStyles.H2}>{ title }</Text>
            { icon }
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    button: {
        width: 150,
        height: 150,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: COLORS.white,
        marginHorizontal: 5,
        marginVertical: 5,
        borderRadius: 10,
        paddingTop: 15,
        paddingBottom: 20
    }
});

export default HomeItem;