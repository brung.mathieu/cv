import React from "react";
import { StyleSheet, View, Text } from "react-native"
import GlobalStyles from "../constants/GlobalStyles";
import COLORS from "../constants/Colors";
import ICONS from "../constants/Icons";

const FormationItem = ({data}) => {
    return (
        <View style={[GlobalStyles.DataWrapper, {marginVertical: 6}]}>

            <View style={styles.topWrapper}>
                <View style={styles.topTitleWrapper}>
                    <View style={[GlobalStyles.Circle, GlobalStyles.Shadow]} />
                    <Text style={GlobalStyles.H3}>{ data.title }</Text>
                </View>

                <Text style={GlobalStyles.H4}>{ data.date }</Text>
            </View>

            <Text style={[GlobalStyles.H4, {textAlign: 'center', color: COLORS.orange}]}>{ data.formationName }</Text>

            <View>
                <View style={styles.lineWrapper}>
                    {ICONS.schoolV2}
                    <View style={styles.textWrapper}>
                        <Text style={GlobalStyles.H4} >{ data.school }</Text>
                        <Text style={GlobalStyles.H4} >{ data.schoolCity }</Text>
                        {
                            data.grade !== '' ?
                                <Text style={GlobalStyles.H4} >Mention "{ data.grade }"</Text>
                                : null
                        }
                    </View>
                </View>

                {
                    data.company !== '' ?
                        <View style={styles.lineWrapper} >
                            {ICONS.companyV2}
                            <View style={styles.textWrapper}>
                                <Text style={GlobalStyles.H4} >{ data.company }</Text>
                                <Text style={GlobalStyles.H4} >{ data.companyCity }</Text>
                            </View>
                        </View>
                        : null
                }

            </View>

        </View>
    )
};

const styles = StyleSheet.create({
    topWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    topTitleWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    lineWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 4
    },
    textWrapper: {
        marginLeft: 10
    }
});

export default FormationItem;