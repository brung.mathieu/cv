import React from 'react';
import {NavigationContainer} from "@react-navigation/native";
import {createNativeStackNavigator} from "@react-navigation/native-stack";
import Home from "./screens/Home";
import Profile from "./screens/Profile";
import Projects from "./screens/Projects";
import Formations from "./screens/Formations";
import Experiences from "./screens/Experiences";
import Hobbies from "./screens/Hobbies";
import Contact from "./screens/Contact";

const Stack = createNativeStackNavigator();

function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Home">
                <Stack.Screen options={ {headerShown: false} } name='Home' component={Home} />
                <Stack.Screen options={ {headerShown: false} } name='Profile' component={Profile} />
                <Stack.Screen options={ {headerShown: false} } name='Projects' component={Projects} />
                <Stack.Screen options={ {headerShown: false} } name='Formations' component={Formations} />
                <Stack.Screen options={ {headerShown: false} } name='Experiences' component={Experiences} />
                <Stack.Screen options={ {headerShown: false} } name='Hobbies' component={Hobbies} />
                <Stack.Screen options={ {headerShown: false} } name='Contact' component={Contact} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default App;