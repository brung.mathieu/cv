const CONTACT_DATA = {
    phone: '+33 6 15 71 96 95',
    email: 'brung.mathieu@gmail.com',
    address: {
      street: '9 rue de la biblanque',
      city: '33320 EYSINES',
    },
    transport: {
        car: true,
        tram: true,
        bus: true,
    },
    social: {
        linkedin: {
            link: 'https://www.linkedin.com/in/mathieu-dumora-brung-29952218a',
            logo: require('../logos/linkedIn.png')
        },
        twitter: {
            link: 'https://twitter.com/MatBrung',
            logo: require('../logos/twitter.png')
        },
        gitlab: {
            link: 'https://gitlab.com/users/brung.mathieu/projects',
            logo: require('../logos/gitLab.png')
        },
    }
};

export default CONTACT_DATA;