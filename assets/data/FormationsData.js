const FORMATIONS_DATA = [
    {
        id: '1',
        title: 'LP DAWIN',
        formationName: 'Développeur d\'Applications Web et Innovation Numérique',
        date: '2021 / 2022',
        school: 'IUT de Bordeaux',
        schoolCity: 'Bordeaux',
        grade: '',
        company: 'CGI',
        companyCity: 'Le Haillan'
    },
    {
        id: '2',
        title: 'BTS SIO SLAM',
        formationName: 'Services Informatiques aux Organisations - Solutions Logicielles et Applications Métiers',
        date: '2019 / 2021',
        school: 'CFA du Lycée Gustave Eiffel',
        schoolCity: 'Bordeaux',
        grade: '',
        company: 'Oxymetal SAS',
        companyCity: 'Bordeaux'
    },
    {
        id: '3',
        title: 'Bac Pro G-A',
        formationName: 'Gestion - Administration',
        date: '2017 / 2019',
        school: 'CFA du Lycée Gustave Eiffel',
        schoolCity: 'Bordeaux',
        grade: 'TRÈS BIEN',
        company: 'CEA CESTA',
        companyCity: 'Le Barp'
    },
    {
        id: '4',
        title: 'Bac Pro TMA',
        formationName: 'Technicien Menuisier Agenceur',
        date: '2012 / 2015',
        school: 'Lycée des métiers Léonard de Vinci',
        schoolCity: 'Blanquefort',
        grade: 'BIEN',
        company: '',
        companyCity: ''
    }
];

export default FORMATIONS_DATA;