const PROJECTS_DATA = [
    {
        id: '1',
        title: 'CV App',
        technos: [
            {
                id: '1',
                logo: require('../logos/reactNative.png')
            },
            {
                id: '2',
                logo: require('../logos/expo.png')
            },
            {
                id: '3',
                logo: require('../logos/css.png')
            },
            {
                id: '4',
                logo: require('../logos/figma.png')
            }
        ],
        ide: require('../logos/webStorm.png'),
        text: 'Application présentant mon CV et dont la v2 permettra de le créer en plus de le visionner.',
        api: '',
    },
    {
        id: '2',
        title: 'Bookshelves',
        technos: [
            {
                id: '1',
                logo: require('../logos/angular.png')
            },
            {
                id: '2',
                logo: require('../logos/bootstrap.png')
            },
            {
                id: '3',
                logo: require('../logos/html.png')
            },
            {
                id: '4',
                logo: require('../logos/firebase.png')
            }
        ],
        ide: require('../logos/webStorm.png'),
        text: 'Site qui permet, une fois inscrit pusi connecté, d\'enregistrer les livres de notre bibliothèque.',
        api: 'Firebase.google.com'
    },
    {
        id: '3',
        title: 'Crypto price traker',
        technos: [
            {
                id: '1',
                logo: require('../logos/reactNative.png')
            },
            {
                id: '2',
                logo: require('../logos/expo.png')
            },
            {
                id: '3',
                logo: require('../logos/css.png')
            },
            {
                id: '4',
                logo: require('../logos/coinGecko.png')
            }
        ],
        ide: require('../logos/webStorm.png'),
        text: 'Application montrant le cours des 30 premières cryptomonnaies et leur résultat à 7 jours.',
        api: 'CoinGecko.com',
    },
    {
        id: '4',
        title: 'Météo',
        technos: [
            {
                id: '1',
                logo: require('../logos/reactNative.png')
            },
            {
                id: '2',
                logo: require('../logos/expo.png')
            },
            {
                id: '3',
                logo: require('../logos/css.png')
            },
            {
                id: '4',
                logo: require('../logos/openWeather.png')
            }
        ],
        ide: require('../logos/webStorm.png'),
        text: 'Application montrant la météo en fonction de la localisation du périphérique et les prévisions sur 4 jours.',
        api: 'OpenWeather.org'
    },
    {
        id: '5',
        title: 'Firebase authentication',
        technos: [
            {
                id: '1',
                logo: require('../logos/reactNative.png')
            },
            {
                id: '2',
                logo: require('../logos/expo.png')
            },
            {
                id: '3',
                logo: require('../logos/css.png')
            },
            {
                id: '4',
                logo: require('../logos/firebase.png')
            }
        ],
        ide: require('../logos/webStorm.png'),
        text: 'Application permettant de s\'inscrire et de se connecter à un backend Firebase.',
        api: 'Firebase.google.com'
    },

    {
        id: '6',
        title: 'Vivons Expo',
        technos: [
            {
                id: '1',
                logo: require('../logos/javaAndroid.png')
            },
            {
                id: '2',
                logo: require('../logos/php.png')
            },

        ],
        ide: require('../logos/androidStudio.png'),
        text: 'Application pour les visiteurs (visualisation) et les exposants (inscription, connexion) des salons \'Vivons\'.',
        api: ''
    },
    {
        id: '7',
        title: 'Citiz',
        technos: [
            {
                id: '1',
                logo: require('../logos/php.png')
            },
            {
                id: '2',
                logo: require('../logos/javaScript.png')
            },
            {
                id: '3',
                logo: require('../logos/html.png')
            }
        ],
        ide: require('../logos/vsCode.png'),
        text: 'Reproduction d\'une partie du site de \'Citiz\', notamment le simulateur de tarif et les stations.',
        api: ''
    }
];

export default PROJECTS_DATA;