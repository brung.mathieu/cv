import ICONS from "../../constants/Icons";

const HOBBIES_DATA = {
    sports: [
        {
            id: '1',
            title: 'Musculation',
            text: 'Je pratique la musculation entre 3 et 5 fois par semaines.',
            icon: ICONS.workout
        },
        {
            id: '2',
            title: 'Handball',
            text: 'Arrété pour cause d\'opération, j\'ai pratiqué le handball en compétition pendant 12 ans, dont 10 à Eysines.',
            icon: ICONS.handball
        },
        {
            id: '3',
            title: 'Running',
            text: 'J\'aime courir de temps en temps, sur des soirées d\'été quand il fait beau ou sur tapis en échauffement pour la musculation',
            icon: ICONS.running
        },
        {
            id: '4',
            title: 'Badminton',
            text: 'Pratiqué pendant 5 ans au collège / lycée, j\'ai eu la chance de pouvoir participer au championnat de France UNSS. Depuis je joue dès que j\'en ai l\'occasion.',
            icon: ICONS.badminton
        },
        {
            id: '5',
            title: 'Marche / randonnée',
            text: 'J\'adore marcher dans la nature, encore plus en montagne, cela peut transformer la balade en challenge sportif.',
            icon: ICONS.hiking
        }
    ],
    activities: [
        {
            id: '1',
            title: 'Travail du bois',
            text: 'Ma passion depuis toujours, je travaille exclusivement avec des outils à main. Certaines de mes réalisations sont visibles sur un compte Instagram dédié, @brung.bois.',
            icon: ICONS.woodworking
        },
        {
            id: '2',
            title: 'Valeting / Detailing',
            text: 'C\'est ma dernière passion naissante, je la pratique sur ma voiture personnelle. ' +
                'Le valeting n\'est autre que du nettoyage automobile et le detailing est l\'amélioration ou la restauration de l\'apparence d\'un véhicule.',
            icon: ICONS.car
        },
        {
            id: '3',
            title: 'Musique',
            text: 'Je n\'en pratique pas (bien que je m\'essaie au piano dès que j\'en ai l\'occasion), mais j\'en écoute à longueur de journée (piano, variété française, pop, rap...).',
            icon: ICONS.music
        },
        {
            id: '4',
            title: 'Cuisine',
            text: 'J\'ai toujours apprécié cuisiné et je le fais au quotidien. Mes meilleurs recettes personnelles => cheesecakes, piñacolada, pancakes...',
            icon: ICONS.cooking
        },
        {
            id: '5',
            title: 'Lecture',
            text: 'Le soir pour me détendre, j\'apprécie lire des livres sur le développement personnel, sur l\'entrepreneuriat et étant plus jeune, j\'ai eu beaucoup lu de mangas.',
            icon: ICONS.book
        }
    ]
}

export default HOBBIES_DATA;