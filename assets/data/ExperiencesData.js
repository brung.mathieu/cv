const EXPERIENCE_DATA = [
    {
        id: '1',
        title: 'CGI - Alternance',
        date_start: 'Septembre 2021',
        date_end: '',
        text: 'Développeur junior Android / Angular.',
        logo: require('../logos/cgi.png'),
    },
    {
        id: '2',
        title: 'Oxymetal - Alternance',
        date_start: 'Septembre 2019',
        date_end: 'Juillet 2020',
        text: 'Développement d’outils VBA, rédaction des modes opératoires, support sur SAGE x3.',
        logo: require('../logos/oxymetal.png'),
    },
    {
        id: '3',
        title: 'CEA CESTA - Alternance',
        date_start: 'Septembre 2017',
        date_end: 'Août 2019',
        text: 'Comptabilité - Gestion sur SAP',
        logo: require('../logos/cea.png'),
    },
    {
        id: '4',
        title: 'Solidarité internationale',
        date_start: 'Février 2015',
        date_end: '',
        text: 'Construction de maison à ossature bois, à Douala, au Cameroun, avec l\'association BASE',
        logo: require('../logos/cameroon.png'),
    },
    {
        id: '5',
        title: 'Olympiades des métiers',
        date_start: 'Mars 2014',
        date_end: '',
        text: 'Participant à l\'épreuve de ménuiserie',
        logo: require('../logos/olympiadesDesMetiers.png'),
    }
];

export default EXPERIENCE_DATA;