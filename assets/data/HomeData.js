import ICONS from "../../constants/Icons";

const HOME_DATA = [
    {
        id: '1',
        title: 'Profil',
        screenName: 'Profile',
        icon: ICONS.user
    },
    {
        id: '2',
        title: 'Projets',
        screenName: 'Projects',
        icon: ICONS.project
    },
    {
        id: '3',
        title: 'Formations',
        screenName: 'Formations',
        icon: ICONS.school
    },
    {
        id: '4',
        title: 'Expériences',
        screenName: 'Experiences',
        icon: ICONS.experience
    },
    {
        id: '5',
        title: 'Loisirs',
        screenName: 'Hobbies',
        icon: ICONS.workout
    },
    {
        id: '6',
        title: 'Contact',
        screenName: 'Contact',
        icon: ICONS.contact
    }
];

export default HOME_DATA;