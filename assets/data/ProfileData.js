const PROFILE_DATA = {
    firstName: 'Mathieu',
    lastName: 'Brung',
    age: '25',
    text: 'Souriant, motivé, travailleur et rigoureux, en dehors des cours et du travail en entreprise, ' +
        'je passe mon temps à faire du sport, écouter de la musique, réaliser des objets en bois ' +
        'ou encore suivre des tutoriels / cours en ligne pour augmenter mes compétences en tant que développeur front web / mobile.'
};

export default PROFILE_DATA;