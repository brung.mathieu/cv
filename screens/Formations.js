import React from "react";
import {StyleSheet, View, Text, ImageBackground, TouchableOpacity, FlatList} from "react-native";
import GlobalStyles from "../constants/GlobalStyles";
import CONST from "../constants/Const";
import FORMATIONS_DATA from "../assets/data/FormationsData";
import Header from "../components/Header";
import FormationItem from "../components/FormationItem";

const Formations = () => {
    return (
        <View style={GlobalStyles.Container}>
            <ImageBackground source={CONST.background} resizeMode="cover" style={GlobalStyles.Background}>

                {/* Header */}
                <Header title={'Formations'} />

                {/* FlatList */}
                <FlatList
                    data={FORMATIONS_DATA}
                    renderItem={({item}) => <FormationItem data={item} />}
                    keyExtractor={(item) => item.id}
                    showsVerticalScrollIndicator={false}
                    style={GlobalStyles.DataContainer}
                />

            </ImageBackground>
        </View>
    )
};

const styles = StyleSheet.create({

});

export default Formations;