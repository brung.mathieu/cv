import React from "react";
import { View, Text, ImageBackground } from "react-native";
import GlobalStyles from "../constants/GlobalStyles";
import CONST from "../constants/Const";
import PROFILE_DATA from "../assets/data/ProfileData";
import Header from "../components/Header";

const Profile = () => {
    return (
        <View style={GlobalStyles.Container}>
            <ImageBackground source={CONST.background} resizeMode="cover" style={GlobalStyles.Background}>

                {/* Header */}
                <Header title={'Profil'} />

                {/* Body */}
                <View style={[GlobalStyles.DataContainer ,GlobalStyles.DataWrapper]}>
                    <Text style={GlobalStyles.Text}>{ PROFILE_DATA.text }</Text>
                </View>

            </ImageBackground>
        </View>
    )
};


export default Profile;