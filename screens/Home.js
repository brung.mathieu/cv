import React from "react";
import {StyleSheet, ImageBackground, View, Text, FlatList, ScrollView} from "react-native";
import GlobalStyles from "../constants/GlobalStyles";
import CONST from "../constants/Const";
import COLORS from "../constants/Colors";
import HOME_DATA from "../assets/data/HomeData";
import HomeItem from "../components/HomeItem";

const Home = () => {

    return (
        <View style={GlobalStyles.Container}>
            <ImageBackground source={CONST.background} resizeMode="cover" style={GlobalStyles.Background}>

                {/* Header */}
                <View style={styles.headerContainer}>
                    <Text style={styles.title}>Mathieu Brung</Text>
                    <Text style={GlobalStyles.H2}>Développeur front</Text>
                    <Text style={GlobalStyles.H2}>web / mobile</Text>
                </View>

                {/* Body */}
                    <View style={[GlobalStyles.DataContainer, {flex: 1, alignItems: 'center'}]}>

                        {/* Flatlist */}
                        <FlatList
                            data={HOME_DATA}
                            renderItem={({item}) => <HomeItem data={item} />}
                            keyExtractor={(item) => item.id}
                            numColumns={2}
                            showsVerticalScrollIndicator={false}
                        />

                    </View>

            </ImageBackground>
        </View>
    )
};

const styles = StyleSheet.create({
    headerContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 60,
        marginHorizontal: 20
    },
    title: {
        color: COLORS.white,
        fontSize: 32,
        fontWeight: 'bold',
        marginBottom: 14
    }
});

export default Home;