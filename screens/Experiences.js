import React from "react";
import {StyleSheet, View, ImageBackground, FlatList, ScrollView} from "react-native";
import GlobalStyles from "../constants/GlobalStyles";
import COLORS from "../constants/Colors";
import CONST from "../constants/Const";
import EXPERIENCE_DATA from "../assets/data/ExperiencesData";
import Header from "../components/Header";
import ExperienceItem from "../components/ExperienceItem";

const Experiences = () => {
    return (
        <View style={GlobalStyles.Container}>
            <ImageBackground source={CONST.background} resizeMode="cover" style={GlobalStyles.Background}>

                {/* Header */}
                <Header title={'Expériences'} />

                <View style={[GlobalStyles.DataContainer, {flex: 1, flexDirection: 'row'}]}>
                    <View style={styles.leftBar} />

                    {/* FlatList */}
                    <FlatList
                        data={EXPERIENCE_DATA}
                        renderItem={({item}) => <ExperienceItem data={item} />}
                        keyExtractor={(item) => item.id}
                        showsVerticalScrollIndicator={false}
                    />
                </View>

            </ImageBackground>
        </View>
    )
};

const styles = StyleSheet.create({
    leftBar: {
        width: 5,
        borderRadius: 4,
        marginLeft: 18,
        backgroundColor: COLORS.orange,
        borderColor: COLORS.orange,
    }
});

export default Experiences;