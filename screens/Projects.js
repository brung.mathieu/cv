import React from "react";
import {StyleSheet, View, Text, Image, ImageBackground, FlatList} from "react-native";
import GlobalStyles from "../constants/GlobalStyles";
import CONST from "../constants/Const";
import PROJECTS_DATA from "../assets/data/ProjectsData";
import Header from "../components/Header";
import ProjectItem from "../components/ProjectItem";

const Projects = () => {
    return (
        <View style={GlobalStyles.Container}>
            <ImageBackground source={CONST.background} resizeMode="cover" style={GlobalStyles.Background}>

                {/* Header */}
                <Header title={'Projets'} />

                {/* GitLab */}
                <View style={[GlobalStyles.DataContainer ,GlobalStyles.DataWrapper, styles.gitlabItem]}>
                    <Image source={require('../assets/logos/gitLab.png')} style={GlobalStyles.LogoTiny} />
                    <View style={styles.textWrapper}>
                        <Text style={GlobalStyles.Text}>Retrouvez tous mes projets sur</Text>
                        <Text style={GlobalStyles.Text}>gitlab.com/users/brung.mathieu/projects</Text>
                    </View>
                </View>

                {/* FlatList */}
                <FlatList
                    data={PROJECTS_DATA}
                    renderItem={({item}) => <ProjectItem data={item} />}
                    keyExtractor={(item) => item.id}
                    showsVerticalScrollIndicator={false}
                />

            </ImageBackground>
        </View>
    )
};

const styles = StyleSheet.create({
    gitlabItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        marginBottom: 16
    },
    textWrapper: {
        alignItems: 'center'
    }
});

export default Projects;