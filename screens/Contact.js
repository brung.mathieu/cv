import React from "react";
import {StyleSheet, View, Text, ImageBackground, Image, TouchableOpacity, Linking} from "react-native";
import GlobalStyles from "../constants/GlobalStyles";
import CONST from "../constants/Const";
import Header from "../components/Header";
import CONTACT_DATA from "../assets/data/ContactData";
import COLORS from "../constants/Colors";
import ICONS from "../constants/Icons";

const Contact = () => {
    return (
        <View style={GlobalStyles.Container}>
            <ImageBackground source={CONST.background} resizeMode="cover" style={GlobalStyles.Background}>

                {/* Header */}
                <Header title={'Contact'} />

                <View style={[GlobalStyles.DataContainer, GlobalStyles.DataWrapper]} >

                    <View style={[styles.lineWrapper, {marginTop: 12}]} >
                        { ICONS.phone }
                        <Text style={GlobalStyles.Text} >{ CONTACT_DATA.phone }</Text>
                    </View>

                    <View style={styles.separator} />

                    <View style={styles.lineWrapper} >
                        { ICONS.email }
                        <Text style={GlobalStyles.Text} >{ CONTACT_DATA.email }</Text>
                    </View>

                    <View style={styles.separator} />

                    <View style={styles.lineWrapper} >
                        { ICONS.location }
                        <View style={{alignItems: 'flex-end'}}>
                            <Text style={GlobalStyles.Text} >{ CONTACT_DATA.address.street }</Text>
                            <Text style={GlobalStyles.Text} >{ CONTACT_DATA.address.city }</Text>
                        </View>
                    </View>

                    <View style={styles.separator} />

                    <View style={styles.iconsWrapper} >
                        {
                            CONTACT_DATA.transport.car === true ?
                                ICONS.car
                                : null
                        }
                        {
                            CONTACT_DATA.transport.tram === true ?
                                ICONS.tram
                                : null
                        }
                        {
                            CONTACT_DATA.transport.bus === true ?
                                ICONS.bus
                                : null
                        }
                    </View>

                    <View style={styles.separator} />

                    <View style={styles.iconsWrapper} >
                        {
                            CONTACT_DATA.social.linkedin.link !== '' ?
                                <TouchableOpacity onPress={() => {
                                    Linking.openURL(CONTACT_DATA.social.linkedin.link)}
                                }>
                                    <Image source={CONTACT_DATA.social.linkedin.logo} style={GlobalStyles.Logo} />
                                </TouchableOpacity>
                                : null
                        }
                        {
                            CONTACT_DATA.social.twitter.link !== '' ?
                                <TouchableOpacity onPress={() => {
                                    Linking.openURL(CONTACT_DATA.social.twitter.link)}
                                }>
                                    <Image source={CONTACT_DATA.social.twitter.logo} style={GlobalStyles.Logo} />
                                </TouchableOpacity>
                                : null
                        }
                        {
                            CONTACT_DATA.social.gitlab.link !== '' ?
                                <TouchableOpacity onPress={() => {
                                    Linking.openURL(CONTACT_DATA.social.gitlab.link)}
                                }>
                                    <Image source={CONTACT_DATA.social.gitlab.logo} style={GlobalStyles.Logo} />
                                </TouchableOpacity>
                                : null
                        }
                    </View>

                </View>

            </ImageBackground>
        </View>
    )
};

const styles = StyleSheet.create({
    separator: {
        alignSelf: 'center',
        width: '100%',
        height: StyleSheet.hairlineWidth,
        backgroundColor: COLORS.gray,
        marginVertical: 12
    },
    lineWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginHorizontal: 12
    },
    iconsWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    }
});

export default Contact;