import React from "react";
import {StyleSheet, View, Text, ImageBackground, FlatList} from "react-native";
import GlobalStyles from "../constants/GlobalStyles";
import CONST from "../constants/Const";
import Header from "../components/Header";
import HOBBIES_DATA from "../assets/data/HobbiesData";
import HobbyItem from "../components/HobbyItem";

const Hobbies = () => {
    return (
        <View style={GlobalStyles.Container}>
            <ImageBackground source={CONST.background} resizeMode="cover" style={GlobalStyles.Background}>

                {/* Header */}
                <Header title={'Loisirs'} />

                <Text style={[GlobalStyles.H2, GlobalStyles.DataContainer, {marginLeft: 20, marginBottom: 10}]}>Sports</Text>
                <View style={GlobalStyles.DataWrapper}>
                    <FlatList
                        data={HOBBIES_DATA.sports}
                        renderItem={({item}) => <HobbyItem data={item} />}
                        keyExtractor={(item) => item.id}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                    />
                </View>


                <Text style={[GlobalStyles.H2, {marginLeft: 20, marginBottom: 10, marginTop: 20}]}>Activités</Text>
                <View style={GlobalStyles.DataWrapper}>
                    <FlatList
                        data={HOBBIES_DATA.activities}
                        renderItem={({item}) => <HobbyItem data={item} />}
                        keyExtractor={(item) => item.id}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                    />
                </View>


            </ImageBackground>
        </View>
    )
};

const styles = StyleSheet.create({

});

export default Hobbies;