import React from "react";
import Entypo from "react-native-vector-icons/Entypo";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Feather from "react-native-vector-icons/Feather";
import Zocial from "react-native-vector-icons/Zocial";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import Fontisto from "react-native-vector-icons/Fontisto";
import {Image} from "react-native";
import GlobalStyles from "./GlobalStyles";


const ICONS = {
    user: <Entypo name='user' size={50} color={'#000'} /> ,
    project: <FontAwesome5 name='laptop-code' size={50} color={'#000'} /> ,
    school: <MaterialCommunityIcons name='school' size={50} color={'#000'} /> ,
    schoolV2: <MaterialCommunityIcons name='school' size={35} color={'#000'} /> ,
    experience: <FontAwesome name='briefcase' size={50} color={'#000'} /> ,
    contact: <MaterialCommunityIcons name='at' size={50} color={'#000'} /> ,
    back: <Feather name='chevron-left' size={35} color={'#000'} /> ,
    phone: <FontAwesome name='phone' size={35} color={'#000'} /> ,
    email: <Zocial name='email' size={35} color={'#000'} /> ,
    location: <MaterialIcons name='location-pin' size={35} color={'#000'} /> ,
    car: <Ionicons name='car-sport' size={35} color={'#000'} /> ,
    bus: <FontAwesome5 name='bus' size={30} color={'#000'} /> ,
    tram: <MaterialCommunityIcons name='tram' size={35} color={'#000'} /> ,
    company: <Ionicons name='business' size={50} color={'#000'} /> ,
    companyV2: <Ionicons name='business' size={35} color={'#000'} /> ,
    woodworking: <Image source={require('../assets/icons/HandPlane.png')} style={GlobalStyles.Logo} /> ,
    book: <FontAwesome5 name='book-reader' size={50} color={'#000'} /> ,
    music: <Fontisto name='music-note' size={50} color={'#000'} /> ,
    cooking: <MaterialCommunityIcons name='chef-hat' size={50} color={'#000'} /> ,
    workout: <Image source={require('../assets/icons/Deadlift.png')} style={GlobalStyles.Logo} /> ,
    handball: <MaterialCommunityIcons name='handball' size={50} color={'#000'} /> ,
    running: <FontAwesome5 name='running' size={50} color={'#000'} /> ,
    badminton: <MaterialCommunityIcons name='badminton' size={50} color={'#000'} /> ,
    hiking: <FontAwesome5 name='hiking' size={50} color={'#000'} /> ,
}

export default ICONS;