const COLORS = {
    orange: '#FFA90F',
    white: '#FAFAFA',
    gray: '#9B9B9B',
    black: '#323232'
}

export default COLORS;