import React from "react";
import {StyleSheet} from "react-native";
import COLORS from "./Colors";

export default StyleSheet.create({
    H1: {
        color: COLORS.orange,
        fontSize: 28,
        fontWeight: 'bold',
        marginTop: 60,
        marginLeft: 30
    },
    H2: {
        color: COLORS.orange,
        fontSize: 24,
        fontWeight: 'bold'
    },
    H3: {
        color: COLORS.black,
        fontSize: 18,
        fontWeight: 'bold'
    },
    H4: {
        color: COLORS.gray,
        fontSize: 14
    },
    Text: {
        color: COLORS.black,
        fontSize: 14,
        textAlign: 'justify'
    },
    Logo: {
        width: 50,
        height: 50
    },
    LogoTiny: {
        width: 35,
        height: 35
    },
    Shadow: {
        shadowColor: COLORS.black,
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowRadius: 10,
        elevation: 5,
    },
    Circle: {
        width: 12,
        height: 12,
        borderColor: COLORS.orange,
        backgroundColor: COLORS.orange,
        borderWidth: 2,
        borderRadius: 6,
        marginRight: 10
    },
    Container: {
        flex: 1
    },
    HeaderContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    DataContainer: {
        marginTop: 40
    },
    DataWrapper: {
        backgroundColor: COLORS.white,
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 14,
        marginHorizontal: 20
    },
    BackButton: {
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 20,
        marginTop: 60,
        width: 50,
        height: 50,
        backgroundColor: COLORS.white,
        opacity: 0.8,
        borderRadius: 10,
    },
    Background: {
        flex: 1
    }
});